<?php

namespace Plaza\Providers;

use Illuminate\Support\ServiceProvider;

class PlazaServiceProvider extends ServiceProvider
{
    
    /**
     * Register any application services.
     */
    public function register(): void
    {
        /** paketeko konfigurazioa, publikatutako konfigurazioarekin mergeatzeko */

        $this->mergeConfigFrom(
            $this->basePath('config/plaza.php'), 'plaza'
        );
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        /** plaza.php konfigurazio fitxategia proiektuan publikatu ahal izateko */

        $this->publishes([
            $this->basePath('config/plaza.php') => config_path('plaza.php'),
            ],
            'plaza-config'
        );

        // TODO : aztertu hauek publikatu behar diren ala ez !
        /** plazaren css eta js-a proiektuan publikatu ahal izateko */

        $this->publishes([
            $this->basePath('resources/css') => $this->basePath('resources/css'),
            ],
            'plaza-css'
        );

        $this->publishes([
            $this->basePath('resources/js') => $this->basePath('resources/js'),
            ],
            'plaza-js'
        );

        /** plazan definitutako oinarrizko view-ak proiektuan publikatu ahal izateko */

        $this->publishes([
            $this->basePath('resources/views/layouts/_base') => config('view.paths')[0] . '/plaza',
            ],
            'plaza-layouts'
        );

        /** Rutak kargatzeko */
        $this->loadRoutesFrom($this->basePath('routes/web.php'));

        /** Migrazioak kargatzeko */
        $this->loadMigrationsFrom($this->basePath('database/migrations'));

        /** Itzulpenen kargatzeko */
        $this->loadTranslationsFrom($this->basePath('lang'), 'plaza');

        /** View-ak kargatzeko */
        $this->loadViewsFrom($this->basePath('resources/views'), 'plaza');
    }

    protected function basePath($path)
    {
        return str_replace('src/Providers', '', __DIR__) . $path;
    }
}
